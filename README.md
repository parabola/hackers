This repository contains information about Parabola contributors.

It previously contains programs for working with that information;
they now live at [parabola-hackers][], and programs for use with
`git-shell`; they now live at
[git-meta.git/git-shell-commands][git-shell-commands].

[parabola-hackers]: https://git.parabola.nu/packages/parabola-hackers.git/
[git-shell-commands]: https://git.parabola.nu/git-meta.git/tree/git-shell-commands

Adding users
============

Users ("hackers") are added by creating a file in the `users/`
directory.  The file should be named `users/${UID}.yml` where UID is
the numeric POSIX user ID for that user.  See [meta-normalize-stdio][]
(part of the `parabola-hackers` package) for a listing of all the
properties you can put in the users file; or look at the existing
files as examples.

[meta-normalize-stdio]: https://git.parabola.nu/packages/parabola-hackers.git/tree/bin/meta-normalize-stdio

*NOTICE* Before pushing, you should run `meta-check` to verify that
 the syntax is correct.  However, when pusing to master, the server
 will also run `meta-check`, and will reject the push if it fails.
 Run `meta-check` by installing the `parabola-hackers` package and
 running

    PARABOLA_HACKERS_YAMLDIR=/path/to/hackers/users /lib/parabola-hackers/meta-check

To add a profile image of a user, add it to the `dev-imgs/` folder.

Images in `dev-imgs/`:
 - MUST be named `${username}.png`
 - SHOULD be 125x125 px
 - SHOULD be run through pngcrush

Doing things with the documentation
===================================

The `parabola-hackers` package contains several programs for doing
things with the data in this repository.  You should install it.

For fuller documentation, see the README of [parabola-hackers][].
But, here some simple commands that you are likely interested in.

Firstly, before using any of these commands, you need to tell them
where to find the files:

	export PARABOLA_HACKERS_YAMLDIR=/path/to/hackers-git/users
	
where `/path/to/hackers-git` is the directory containing this README
file.  Alternatively, you may set `yamldir` in
`/etc/parabola-hackers.yml` (the environment variable takes precedence
over the config file).

Check the integrity and formatting of the files:

    /usr/lib/parabola-hackers/meta-check
	
PLEASE, PLEASE, OH PLEASE: run the above command before committing to
this repository.

Create a tarball to be the source of the parabola-keyring package:

    /usr/lib/parabola-hackers/pacman-make-keyring V=$(date -u +%Y%m%d)
